<?php

namespace app\doctor\controller;

use think\Controller;

/**
 * Description of IndexController
 *
 * @author lumy
 */
class IndexController extends Controller {

    public function index() {
        return $this->fetch();
    }

    public function edit() {
        return $this->fetch();
    }
}
