<?php

function post_WsData($data) {
    defined('WebsocketKey') or include CMF_DATA . 'websocket.php';
    $data['sercret'] = WebsocketKey;
    $xpsv = WebsocketUrl;
    $c = new WebSocket\Client($xpsv);
    $c->send(json_encode($data));
    $resp = $c->receive();
    return $resp;
}
