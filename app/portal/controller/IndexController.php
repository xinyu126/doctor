<?php

namespace app\portal\controller;

use think\Controller;

/**
 * Description of IndexController
 *
 * @author lumy
 */
class IndexController extends Controller {

    public function index() {
        return $this->fetch();
    }

    
    public function test(){
        //A 发送消息给B的流程:
        //客户端A发送消息给php, 
        //php 存入数据库, 随后通过此接口发送给 websockt
        //websockt发送消息给客户端B
        $resp = post_WsData(['from'=>'A_id','data'=>['type'=>'消息类型'],'send_to'=>'B_id']);
        var_dump($resp);
    }
}
